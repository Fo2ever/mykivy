from kivy.app import App
from kivy.uix.widget import Widget
from kivy.graphics import Rectangle
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout

class GameWidget(Widget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._keyboard = Window.request_keyboard(
            self._on_keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_key_down)
        self._keyboard.bind(on_key_up=self._on_key_up)

        self.pressed_keys = set()
        Clock.schedule_interval(self.move_step, 0)

        with self.canvas:
            self.hero = Rectangle(source='snorlax.png', pos=(10, 10), size=(100, 100))

    def _on_keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_key_down)
        self._keyboard = None

    def _on_key_up(self, keyboard, keycode):
        text = keycode[1]
        print('up', text)
        
        if text in self.pressed_keys:
            self.pressed_keys.remove(text)

    def _on_key_down(self, keyboard, keycode, text, modifiers):
        print('down', text)
        self.pressed_keys.add(text)

    def move_step(self, dt):
        cur_x = self.hero.pos[0]
        cur_y = self.hero.pos[1]
        step = 200 * dt
        if 'w' in self.pressed_keys:
            cur_y += step
        if 's' in self.pressed_keys:
            cur_y -= step
        if 'a' in self.pressed_keys:
            cur_x -= step
        if 'd' in self.pressed_keys:
            cur_x += step
        self.hero.pos = (cur_x, cur_y)


class MyApp(App):
    def build(self):
        return GameWidget()


if __name__ == '__main__':
    app = MyApp()
    app.run()




# from kivy.app import App
# from kivy.uix.image import Image
# from kivy.core.window import Window
# from kivy.uix.screenmanager import ScreenManager, Screen
# from kivy.uix.behaviors import ButtonBehavior
# from kivy.uix.floatlayout import FloatLayout
# from kivy.uix.screenmanager import FallOutTransition


# gamelayout = FloatLayout(size=(300, 300))


# class Game(Screen):
#     pass    


# class IconButton(ButtonBehavior, Image):
#     pass

# class MoveableImage(Image):

#     def __init__(self, **kwargs):
#         super(MoveableImage, self).__init__(**kwargs)
#         self._keyboard = Window.request_keyboard(None, self)
#         if not self._keyboard:
#             return
#         self._keyboard.bind(on_key_down=self.on_keyboard_down)
#         self._keyboard.bind(on_key_up=self.on_keyboard_up)
#         self.size_hint = (.11, .11)
#         self.y = (Window.height/2.1)
#         self.x = (Window.height/1.7)
#         self.app = App.get_running_app()



#     def on_keyboard_down(self, keyboard, keycode, text, modifiers):
#         print('down', text)
#         if keycode[1] == 'left':
#             self.source = 'snorlax.png'
#             self.anim_delay=.20
#             if self.x < (Window.width * 0):
#                 self.app.test.x += 4
#             else:
#                 self.x -= 6
#         elif keycode[1] == 'down':
#             self.source ='snorlax.png'
#             self.anim_delay=.20
#             if self.y < (Window.height * 0):
#                 self.app.test.y += 4
#             else:
#                 self.y -= 6
#         return True

#     def on_keyboard_up(self, keyboard, keycode):
#         if keycode[1] == 'left':
#             self.source = 'snorlax.png'
#         elif keycode[1] == 'down':
#             self.source ='snorlax.png'
#         else:
#             return False
#         return True



# class gameApp(App):
#     def build(self):
#         global sm 
#         sm = ScreenManager()
#         game = Game(name='game')
#         sm.add_widget(game)
#         hero = MoveableImage(source='snorlax.png', pos=(75, 40))
#         self.hero2 = Image(source='snorlax.png', pos=(100, 200))
#         gamelayout.add_widget(hero)
#         game.add_widget(gamelayout)
#         return sm






# if __name__ == '__main__':
#     gameApp().run()