# from kivy.app import App
# from kivy.uix.scatter import Scatter
# from kivy.uix.label import Label
# from kivy.uix.floatlayout import FloatLayout
# class TutorialApp(App):
#  def build(self):
#     f = FloatLayout()
#     s = Scatter()
#     l = Label(text="Hello!",
#         font_size=150)
#     f.add_widget(s)
#     s.add_widget(l)
#     return f
# if __name__ == "__main__":
#     TutorialApp().run()



# from kivy.app import App
# from kivy.uix.scatter import Scatter
# from kivy.uix.label import Label
# from kivy.uix.floatlayout import FloatLayout
# from kivy.uix.textinput import TextInput
# from kivy.uix.boxlayout import BoxLayout

# class TutorialApp(App):
#     def build(self):
#         b = BoxLayout()
#         t = TextInput(font_size=150)
#         f = FloatLayout()
#         s = Scatter()
#         l = Label(text="Hello!",
#                 font_size=150)

#         f.add_widget(s)
#         s.add_widget(l)

#         b.add_widget(f)
#         b.add_widget(t)
#         return b
# if __name__ == "__main__":
#     TutorialApp().run()


from kivy.app import App
from kivy.uix.scatter import Scatter
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
class TutorialApp(App):
    def build(self):
        b = BoxLayout(orientation ='vertical')
        t = TextInput(text='Hello',
                    font_size=150)
        l = Label(font_size=150)
        
        b.add_widget(t)
        b.add_widget(l)
        t.bind(text=l.setter('text'))
        return b
if __name__ == "__main__":
    TutorialApp().run() 

# from kivy.app import App
# from kivy.core.window import Window
# from kivy.uix.scatter import Scatter
# from kivy.uix.label import Label
# from kivy.uix.floatlayout import FloatLayout
# from kivy.uix.textinput import TextInput
# from kivy.uix.boxlayout import BoxLayout
# class TutorialApp(App):
#     def build(self):
#         b = BoxLayout(orientation='vertical') # ---> 
#         t = TextInput(text='Hello', # ---> เป็น block ที่รับ input 
#                 font_size=150,  # ---> กำหดขนาดของ font ตำแหน่ง
#                 size_hint_y=None, # ---> ข้อความเริ่มต้นคือ Hello
#                 height=200)
#         l = Label(font_size=150, # ---> เป็นข้อความอีกชุดนึงซึ่งกำหนดขนาดและตำแหน่งไว้ที่ s = Scatter
#                     text='Hello') 
#         f = FloatLayout() # ---> สร้าง block มาอีก 1 block เพื่อไว้แสดงข้อความ
#         size = Window.size
#         s = Scatter(pos=(90, size[1]/2 - 200)) # ---> ทำให้ข้อความเลื่อนได้ และกำหนดตำแหน่งและขนาดของข้อความ
  
#         f.add_widget(s) # ---> นำ s มาใส่ไว้ใน f
#         s.add_widget(l) # ---> นำ l มาใส่ไว้ใน s ซึ่ง s อยู่ใน f
#         b.add_widget(t) # ---> นำ t มาใส่ไว้ใน b
#         b.add_widget(f) # ---> นำ f มาใส่ไว้ใน b ซึ่งเป็น Boxlayout หลัก
#         t.bind(text=l.setter('text')) # ---> ทำให้ข้อความที่รับจาก input จะให้ข้อความใน label เปลี่ยนด้วย
#         return b

# if __name__ == "__main__":
#     TutorialApp().run()
