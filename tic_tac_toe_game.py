from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.properties import NumericProperty, StringProperty
import itertools
import sys


Builder.load_file('tic_tac_toe_game.kv')
class ButtonOne(Button):
    def click(self):
        game_screen = self.parent.parent.parent.parent
        if game_screen.state % 2 == 0:
            self.text = 'O'
            game_screen.state = 1
            # self.root.ids.score.text = "O's Turn!"
        else:
            self.text = 'X'
            game_screen.state = 0
            # self.root.ids.score.text = "X's Turn!"

        self.disabled = True
        game_screen.click_count += 1

        if game_screen.click_count >= 5:
            game_screen.check_anwser()


class GameScreen(Screen):
    state = NumericProperty(0)
    click_count = NumericProperty(0)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def check_anwser(self):
        print('Ckeck answer')
        # for i in range(3):
        #     for b in range(3):
        #         print(self.ids[f"{i}{b}"].text)

        # keys = ["".join(x) for x in itertools.product("012", repeat=2)]
    # def winnerO(self):
        winner = [
            ('00', '01', '02'),
            ('10', '11', '12'),
            ('20', '21', '22'),
            ('00', '10', '20'),
            ('10', '11', '12'),
            ('20', '21', '22'),
            ('00', '11', '22'),
            ('02', '11', '20'),
            ('01', '11', '21')
        ]

        for i,j,k in winner:
            if self.ids[f'button_{i}'].text and (
                self.ids[f'button_{i}'].text
                == self.ids[f'button_{j}'].text
                == self.ids[f'button_{k}'].text
            ):
                print(i, self.ids[f"button_{i}"].text, 'win')
                message = self.ids["message"]
                message.text = f'Player: {self.ids[f"button_{i}"].text} Win'
                message.color = (0, 1, 0, 1)
                break
        for position,position1,position2 in winner:
            if isinstance(position,position1,position2, int):
                return False

        print(position, self.ids[f"button_{position}"].text, 'Tie')
        message = self.ids["message"]
        message.text = f'Player: {self.ids[f"button_{position}"].text} Tie'
        message.color = (0, 1, 0, 1)

    # def is_int(s):
    #     try:
    #         int(s)
    #         return True
    #     except ValueError:
    #         return False

    # def is_tie(winnerO,is_int):
    #     for position in winnerO:
    #         if is_int(position):
    #             return False
    #     print('The game is a tie.')
    #     sys.exit()


    


class TicTacToeApp(App):
    def build(self):
        # self.theme.cls.them_style = 'Drak'
        # self.theme.cls.primary_palette = 'BlueGary'
        sm = ScreenManager()
        sm.add_widget(GameScreen(name="game"))
        return sm

    # def presser(self):
    #     pass
    # def restart(self):
    #     pass

if __name__ == "__main__":
    TicTacToeApp().run()
 